> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Veronica Petters

### Assignment 1 Requirements

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above);
* Screenshot of running http://localhost:9999 (#2 above, Step #4(b) in tutorial);
* git commands with short descriptions;
* Bitbucket repos links: a) this assignment and b) the completed tutorial above (bitbucketstationlocations and myteamquotes);

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Creates a new repository.
2. git status - Displays the state of the directory and the staging status.
3. git add - Adds a change to directory/staging.
4. git commit - Commits stages to the project history.
5. git push - Pushes commits made on a locol repos to a remote repos.
6. git pull - Merges the remote repos with the local repos in a single command.
7. git remote - Create, view and delete connections to other repos. Similar to a bookmark.

References: https://www.atlassian.com/git/tutorials/syncing

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost:9999*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/veronicapetters/bitbucketstationlocations "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/veronicapetters/myteamquotes "My Team Quotes Tutorial")

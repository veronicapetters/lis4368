> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Veronica Petters

### Assignment 5 Requirements

*Two Parts:*

1. Server-side validation
2. Edit files


#### README.md file should include the following items:

*Screenshot of the Customer Table:

> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshot and Links:
*Valid User Form Entry*
![Valid User Form Entry](img/entry.png)

*Passed Validation*:
![Passed Validation](img/passed.png)

*Display Data*
![Display Data](img/data.png)

*Modify Form*
![Modify Form](img/form.png)

*Modified Data*
![Modified Data](img/mod_data.png)

*Delete Warning*
![Delete Warning](img/delete.png)

*Associated Database Changes (Select, Insert, Update, Delete)*
![Associated Database Changes (Select, Insert, Update, Delete)](img/associated.png)

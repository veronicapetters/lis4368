> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 Advanced Web Applications Development

## Veronica Petters

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitucket repo
    - Complete Bitbucket tutorials
    (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MYSQL
    - Develop and deploy WebApps
    - Provide screenshots of installations

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Install MYSQL Workbench
    - Create local connection
    - Enter data into ERD
    - Forward engineer to create script

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Edit jsp
    - Screenshots of jsp
    - Update home page

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Edit JSP
    - Import new files
    - Update Client form  

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Server-side validation

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Complete CRUD

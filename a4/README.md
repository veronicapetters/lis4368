> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Veronica Petters

### Assignment 4 Requirements

*Three Parts:*

1. Edit WEB-INFS
2. Update customer info in jsp's
3. Link to servlet


#### README.md file should include the following items:

*Screenshot of the Customer Table:

> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshot and Links:
*Failed Validation*
![Failed Validation](img/failed.png)

*Passed Validation*:
![Passed Validation](img/passed.png)

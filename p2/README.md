> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Veronica Petters

### Project 2 Requirements

*Two Parts:*

1. Change Files

#### README.md file should include the following items:

*Screenshot passed Validation
*Screenshot of failed Validation
*Screenshot of changed home page

> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshot and Links:

*Form Validation*:
![Failed Validation](img/valid.png)

*Passed Validation*
![Passed Validation](img/passed.png)

*Display Data*
![Display Data](img/display.png)

*Modify Form*
![Modify Form](img/modify.png)

*Modified Data*
![Modified Data](img/modified.png)

*Delete Warning*
![Delete Warning](img/delete.png)

*Database Changes*
![Database Changes](img/database.png)

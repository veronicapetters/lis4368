> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Veronica Petters

### Project 1 Requirements

*Four Parts:*

1. Change JSP
2. Validate JSP

#### README.md file should include the following items:

*Screenshot passed Validation
*Screenshot of failed Validation
*Screenshot of changed home page

> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshot and Links:
*LIS4368 Portal (Main/SplashPage)*
![LIS4368 Portal (Main/SplashPage)](img/home.png)

*Failed Validation*:
![Failed Validation](img/failure.png)

*Passed Validation*
![Passed Validation](img/success.png)

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Veronica Petters

### Assignment 2 Requirements

*Three Parts:*

1. MySQL intallment and download
2. Develop and deploy servlet
3. Chapter Questions (Chs 5 & 6)

#### README.md file should include the following items:

* Assessment links:
* http://localhost:9999/hello
* http://localhost:9999/hello/HelloHome.html
* http://localhost:9999/hello/sayhello
* http://localhost:9999/querybook.html
* http://localhost:9999/hello/sayhi

* Only one screenshot of the query results from http://localhost:9999/hello/querybook.html

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshot:

*Screenshot of query results http://localhost:9999/querybook.html*:

![AMPPS Installation Screenshot](img/queryresults.png)
